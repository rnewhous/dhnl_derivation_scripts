import os

command = """
Reco_tf.py \\
--inputAODFile /data/hnl/KShort/mc16_13TeV.361024.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ4W.merge.DAOD_RPVLL.e3668_s3126_r10848_r10706/DAOD_RPVLL.15780180._000036.pool.root.1 \\
--outputDAODFile output.pool.root \\
--preExec "from AthenaCommon.AlgSequence import AlgSequence; topSequence=AlgSequence(); topSequence += CfgMgr.xAODMaker__DynVarFixerAlg(\"BTaggingELFixer\", Containers = [ \"BTagging_AntiKt4EMTopoAux.\" ] );" \
--reductionConf SUSY15  \\
"""
os.system(command)
