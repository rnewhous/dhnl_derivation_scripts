Reco_tf.py \
--inputAODFile /data/newhouse/HNL/dijet/mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.merge.DAOD_RPVLL.e6337_e5984_s3126_r11915_r11748/DAOD_RPVLL.21278224._000050.pool.root.1 \
--outputDAODFile output.pool.root \
--preExec "from AthenaCommon.AlgSequence import AlgSequence; topSequence=AlgSequence(); topSequence += CfgMgr.xAODMaker__DynVarFixerAlg('BTaggingELFixer', Containers = [ 'BTagging_AntiKt4EMTopoAux.' ] );" \
--reductionConf SUSY15  
mv DAOD_SUSY15.output.pool.root /data/newhouse/HNL/dijet/mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.merge.DAOD_RPVLL.e6337_e5984_s3126_r11915_r11748/
