# Run arguments file auto-generated on Thu Sep  3 10:44:20 2020 by:
# JobTransform: AODtoDAOD
# Version: $Id: trfExe.py 792052 2017-01-13 13:36:51Z mavogel $
# Import runArgs class
from PyJobTransforms.trfJobOptions import RunArguments
runArgs = RunArguments()
runArgs.trfSubstepName = 'AODtoDAOD' 

runArgs.preExec = ["from AthenaCommon.AlgSequence import AlgSequence; topSequence=AlgSequence(); topSequence += CfgMgr.xAODMaker__DynVarFixerAlg('BTaggingELFixer', Containers = [ 'BTagging_AntiKt4EMTopoAux.' ] );"]
runArgs.reductionConf = ['SUSY15']

# Explicitly added to process all events in this step
runArgs.maxEvents = -1

# Input data
runArgs.inputAODFile = ['/data/newhouse/HNL/dijet/mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.merge.DAOD_RPVLL.e6337_e5984_s3126_r11915_r11748/DAOD_RPVLL.21278224._000050.pool.root.1']
runArgs.inputAODFileType = 'AOD'
runArgs.inputAODFileNentries = 5000L
runArgs.AODFileIO = 'input'

# Output data
runArgs.outputDAOD_SUSY15File = 'DAOD_SUSY15.output.pool.root'
runArgs.outputDAOD_SUSY15FileType = 'AOD'

# Extra runargs

# Extra runtime runargs

# Literal runargs snippets
