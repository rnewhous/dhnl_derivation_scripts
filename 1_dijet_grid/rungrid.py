import os
import samples

for campaign in [
    'mc16a', 
    'mc16d', 
    'mc16e', 
    ]:
    for sample in samples.samples[campaign]:
        outds = '.'.join(sample.split('.')[1:3])+'.'+campaign+'.DAOD_SUSY15'
        command = """
pathena --trf \\
'Reco_tf.py \\
--inputAODFile %IN \\
--outputDAODFile output.pool.root \\
--preExec "from AthenaCommon.AlgSequence import AlgSequence; topSequence=AlgSequence(); topSequence += CfgMgr.xAODMaker__DynVarFixerAlg(\\\"BTaggingELFixer\\\", Containers = [ \\\"BTagging_AntiKt4EMTopoAux.\\\" ] );" \\
--reductionConf SUSY15 ' \\
--extOutFile DAOD_SUSY15.output.pool.root \\
--osMatching CentOS7 \\
--inDS {sample} \\
--nFiles=100 \\
--outDS user.rnewhous.{outds}
        """.format(sample=sample, outds=outds)
        print (command)
        # break

